#!/bin/bash 

ID=$(id -u)     #checkinng if it is root user or not  
LOG="/tmp/proj.log"
FUSER="student"

APACHE_VERSION="8.5.78"
TOMCAT_URL="https://dlcdn.apache.org/tomcat/tomcat-8/v$APACHE_VERSION/bin/apache-tomcat-$APACHE_VERSION.tar.gz"
WAR_FILE="https://devops-cloudcareers.s3.ap-south-1.amazonaws.com/student.war"
JAR_FILE="https://devops-cloudcareers.s3.ap-south-1.amazonaws.com/mysql-connector.jar"

echo $ID

    if [ $ID -ne 0 ] ; then          
        echo -e "\e[32m Please login as Root user \e[0m"
        exit 2
    fi

stat () {

    if [ $1 -eq 0 ] ; then # if i want to check upar ke commands run hua ki nahi bol ke 
    echo -e "\e[33m Sucessfull \e[0m"

else 
    echo -e "\e[31m fail \e[0m" 
fi
}

#FORNT END SETUP 

echo -n "Installing web server :"
yum install httpd -y &> $LOG # WHEN WE WANT OUTPUT SCREEN TO BE CLEAN AND MOVE IT TO LOG FINE 
stat $?


echo -n "Creating Proxy Config: "
echo 'ProxyPass "/student" "http://localhost:8080/student"
ProxyPassReverse "/student"  "http://localhost:8080/student" ' > /etc/httpd/conf.d/proxy.conf  
stat $?


echo -n "Set up student index file "
curl -s https://devops-cloudcareers.s3.ap-south-1.amazonaws.com/index.html -o /var/www/html/index.html
stat $?

echo -n "Starting a Web Service"
systemctl enable httpd
systemctl start httpd
stat $?

#Backup Configuration 

echo -n "Installing the JAVA"
yum install java -y &> $LOG
stat $? 

echo -n "Creating $FUSER functional User"

id $FUSER &> $LOG 

if [ $? -eq 0 ] ; then # if i want to check upar ke commands run hua ki nahi bol ke 
    echo -e "\e[33m Skipping as already a user present \e[0m"

else 
    useradd $FUSER &> $LOG 
        stat $?
fi    


echo -n "Downloading the tomcat"  #this needs to be downloaded in student folder 
cd /home/$FUSER
wget $TOMCAT_URL &>> $LOG
tar -xf /home/student/apache-tomcat-$APACHE_VERSION.tar.gz &>> $LOG
chown -R $FUSER:$FUSER apache-tomcat-$APACHE_VERSION &>> $LOG

stat $?

echo -n "Downloading the $FUSER War file: "
cd apache-tomcat-$APACHE_VERSION
wget $WAR_FILE -O webapps/student.war &>> $LOG
stat $?

echo -n "Downloading the JDBC Jar file: "
wget $JAR_FILE -O lib/mysql-connector.jar &>> $LOG 
chown $FUSER:$FUSER lib/mysql-connector.jar &>> $LOG
stat $?


echo -n "Starting Tomcat: "
sh bin/startup.sh  &>> $LOG
stat $?










 